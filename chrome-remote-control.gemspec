# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'chrome-remote-control/version'

Gem::Specification.new do |spec|
  spec.name          = 'chrome-remote-control'
  spec.version       = ChromeRemoteControl::VERSION
  spec.authors       = ['Jonas Jasas', 'Luismi Cavalle']
  spec.email         = ['jonas.jasas@gmail.com', 'luismi@lmcavalle.com']

  spec.summary       = 'ChromeRemote is a client implementation of the Chrome DevTools Protocol in Ruby'
  spec.homepage      = 'https://gitlab.com/jonas.jasas/chrome-remote-control'
  spec.license       = 'MIT'

  # spec.files         = `git ls-files -z`.split('\x0').reject do |f|
  #   f.match(%r{^(test|spec|features)/})
  # end
  spec.require_paths = ['lib']

  spec.add_dependency 'websocket-driver', '~> 0.6'

  spec.add_development_dependency 'bundler', '~> 1.15'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'em-websocket', '~> 0.5'
  spec.add_development_dependency 'byebug'
  spec.add_development_dependency 'webmock'
end
