require "chrome-remote-control/version"
require "chrome-remote-control/client"
require "json"
require "net/http"

module ChromeRemoteControl
  class << self
    DEFAULT_OPTIONS = {
        host: "localhost",
        port: 9222
    }

    def client(options = {})
      options = DEFAULT_OPTIONS.merge(options)

      Client.new(get_ws_url(options))
    end

    def client_endpoint(ws_url, web_socket_options = {})
      Client.new(ws_url, web_socket_options)
    end

    def get_browser_ws_url(host = 'localhost', port = 9222)
      response = Net::HTTP.get(host, '/json/version', port)
      JSON.parse(response)["webSocketDebuggerUrl"]
    end

    private

    def get_ws_url(options)
      response = Net::HTTP.get(options[:host], "/json", options[:port])
      # TODO handle unsuccesful request
      response = JSON.parse(response)

      first_page = response.find {|e| e["type"] == "page"}
      # TODO handle no entry found
      first_page["webSocketDebuggerUrl"]
    end
  end
end
